/*
    *
    * This file is a part of CoreStats.
    * A real time system resource viewer for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#include <cprime/cprime.h>
#include <csys/disk_info.h>

#include "usagegraph.h"
#include "presources.h"
#include "ui_presources.h"

presources::presources(QWidget *parent) : QWidget(parent)
{
	im = InfoManager::ins();
	mTimer = new QBasicTimer();

	init();
}

presources::~presources()
{
    // Nothing does
}

void presources::init()
{
	QVBoxLayout *chartsLayout = new QVBoxLayout();

	/* n cores */
    mPlotCpu = new UsageGraph("CPU Load", "%", im->getCpuCoreCount(), this);

	/* Ram and Swap */
    mPlotMemory = new UsageGraph("Memory Usage", "%", 2, this);

	/* 1 minute, 5 minutes,, 15 minutes */
    mPlotCpuLoadAvg = new UsageGraph("CPU Load Averages", "%", 3, this);
    mPlotCpuLoadAvg->setLegend(QStringList() << "15 Minutes" << "5 Minutes" << "1 Minute");

	/* Read and Write */
    DiskInfo disk;

    mPlotDiskIO = new UsageGraph(QString("Disk Read/Write(%1)").arg(disk.getRootFolderDisk()), "B", 2, this);
    mPlotDiskIO->setLegend(QStringList() << "Read: 0 B" << "Write: 0 B");
    mPlotDiskIO->setYRange(0, 100);

	/* Upload and Download */
    mPlotNetwork = new UsageGraph("Network Speed", "B/s", 2, this);
    mPlotNetwork->setLegend(QStringList() << "Download: 0 B" << "Upload: 0 B");
    mPlotNetwork->setYRange(0, 100);

    chartsLayout->addWidget(mPlotCpu);
    chartsLayout->addWidget(mPlotMemory);
    chartsLayout->addWidget(mPlotCpuLoadAvg);
    chartsLayout->addWidget(mPlotDiskIO);
    chartsLayout->addWidget(mPlotNetwork);

    setLayout(chartsLayout);

    connect(mPlotCpu, SIGNAL(zoomChart()), this, SLOT(zoomChart()));
    connect(mPlotCpuLoadAvg, SIGNAL(zoomChart()), this, SLOT(zoomChart()));
    connect(mPlotDiskIO, SIGNAL(zoomChart()), this, SLOT(zoomChart()));
    connect(mPlotMemory, SIGNAL(zoomChart()), this, SLOT(zoomChart()));
    connect(mPlotNetwork, SIGNAL(zoomChart()), this, SLOT(zoomChart()));

	/* 1 second timer */
    mTimer->start(1000, this);
}

void presources::timerEvent(QTimerEvent *tEvent)
{
    if (tEvent->timerId() == mTimer->timerId()) {
        /* Update the charts */
        updateCpuChart();
        updateMemoryChart();
        updateCpuLoadAvg();
        updateDiskReadWrite();
        updateNetworkChart();
	}

	tEvent->accept();
}

void presources::updateCpuChart()
{
	QList<int> percents = im->getCpuPercents();

    if (percents.count() == im->getCpuCoreCount() + 1) {
		percents.removeFirst();
    }

	UsageValues cpuPercents;
	QStringList cpuLegends;

    for (int i = 0; i < im->getCpuCoreCount(); i++) {
        cpuPercents << static_cast<double>(percents.value(i));
        cpuLegends << QString("CPU %1: %2%").arg(i).arg(percents.value(i));
	}

    mPlotCpu->addValues(cpuPercents);
    mPlotCpu->setLegend(cpuLegends);
}

void presources::updateMemoryChart()
{
	im->updateMemoryInfo();
	UsageValues memUsage;
	QStringList legends;

	// Memory
    if (im->getMemTotal()) {
		memUsage << 100 * im->getMemUsed() / im->getMemTotal();
        legends << QString("RAM: %1 of %2").arg(CPrime::FileFunc::formatSize(im->getMemUsed())).arg(CPrime::FileFunc::formatSize(im->getMemTotal()));
    } else {
		memUsage << 0;
        legends << QString("RAM: 0 b of 0 b");
	}

	// Swap
    if (im->getSwapTotal()) {
		memUsage << 100.0 * im->getSwapUsed() / im->getSwapTotal();
        legends << QString("Swap: %1 of %2").arg(CPrime::FileFunc::formatSize(im->getSwapUsed())).arg(CPrime::FileFunc::formatSize(im->getSwapTotal()));
    } else {
		memUsage << 0;
        legends << QString("Swap: 0 b of 0 b");
	}

    mPlotMemory->addValues(memUsage);
    mPlotMemory->setLegend(legends);
}

void presources::updateCpuLoadAvg()
{
	UsageValues cpuLoadAvgs = im->getCpuLoadAvgs();
    mPlotCpuLoadAvg->addValues(cpuLoadAvgs);
}

void presources::updateDiskReadWrite()
{
    QList<quint64> diskReadWrite = im->getDiskIO();

    quint64 rBytes = prevRBytes ? diskReadWrite.at(0) - prevRBytes : 0;
    quint64 wBytes = prevWBytes ? diskReadWrite.at(1) - prevWBytes : 0;

    prevDiskMax = qMax(prevDiskMax, CPrime::FileFunc::maxYForSize(qMax(rBytes, wBytes)));
    mPlotDiskIO->setLegend(QStringList() << QString("Read: %1 ").arg(CPrime::FileFunc::formatSize(rBytes))
                           << QString("Write: %1 ").arg(CPrime::FileFunc::formatSize(wBytes)));
    mPlotDiskIO->setYRange(0, prevDiskMax);
    mPlotDiskIO->addValues(UsageValues() << rBytes << wBytes);

    prevRBytes = diskReadWrite.at(0);
    prevWBytes = diskReadWrite.at(1);
}

void presources::updateNetworkChart()
{
	quint64 RecvBytes = im->getRXbytes();
	quint64 TrnsBytes = im->getTXbytes();

	quint64 RecvSpeed = prevRXBytes ? RecvBytes - prevRXBytes : 0;
	quint64 TrnsSpeed = prevTXBytes ? TrnsBytes - prevTXBytes : 0;

    prevNetMax = qMax(prevNetMax, CPrime::FileFunc::maxYForSize(qMax(RecvSpeed, TrnsSpeed)));
    mPlotNetwork->setLegend(QStringList() << QString("Download: %1 ").arg(CPrime::FileFunc::formatSize(RecvSpeed))
                            << QString("Upload: %1 ").arg(CPrime::FileFunc::formatSize(TrnsSpeed)));
    mPlotNetwork->setYRange(0, prevNetMax);
    mPlotNetwork->addValues(UsageValues() << RecvSpeed << TrnsSpeed);

	prevRXBytes = RecvBytes;
	prevTXBytes = TrnsBytes;
}

void presources::zoomChart()
{
    UsageGraph *chart = qobject_cast<UsageGraph *>(sender());

    /* Show one graph zoomed */
    if (chart->zoom()) {
        mPlotCpu->setVisible(mPlotCpu == chart);
        mPlotCpuLoadAvg->setVisible(mPlotCpuLoadAvg == chart);
        mPlotDiskIO->setVisible(mPlotDiskIO == chart);
        mPlotMemory->setVisible(mPlotMemory == chart);
        mPlotNetwork->setVisible(mPlotNetwork == chart);
    } else {
		mPlotCpu->show();
		mPlotCpuLoadAvg->show();
		mPlotDiskIO->show();
		mPlotMemory->show();
		mPlotNetwork->show();
	}
}

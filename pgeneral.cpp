/*
    *
    * This file is a part of CoreStats.
    * A real time system resource viewer for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/


#include <QStringListModel>
#include <QScreen>
#include <QDebug>
#include <QDir>
#include <QStorageInfo>

#include <cprime/cprime.h>
#include <csys/system_info.h>

#include "pgeneral.h"
#include "ui_pgeneral.h"


pgeneral::pgeneral(QWidget *parent) : QWidget(parent), ui(new Ui::pgeneral),
    timer(new QBasicTimer()),
	im(InfoManager::ins())
{
	ui->setupUi(this);

    timer->start(1000, this);

    updateInfo();
	systemInformationInit();
}

pgeneral::~pgeneral()
{
    delete ui;
}

void pgeneral::timerEvent(QTimerEvent *event)
{
    if (event->timerId() == timer->timerId()) {
        updateInfo();
    }
}

void pgeneral::updateInfo()
{
	// initialization
	updateCpuBar();
	updateMemoryBar();
    updateNetworkBar();
    updateStorageBar();
}

void pgeneral::systemInformationInit()
{
    QSize scSize = qApp->primaryScreen()->size();
    QString scr = QString("%1 x %2").arg(scSize.width()).arg(scSize.height());

	// get system information
    QSysInfo sys;
    SystemInfo sysInfo;
    QStorageInfo storage = QStorageInfo::root();

	QStringList left;
    left << "User Name" << "Platform" << "Host" << "Kernel Release" << "Root File System"
         << "CPU Model" << "CPU Architecture" << "CPU Speed" << "CPU Cores" << "RAM Size" << "Display Size"
         << "Bootup time" << "Uptime" << "Qt Toolkit Version";

	QStringList right;
    right << sysInfo.getUsername() << sysInfo.getPlatform() << sysInfo.getDistribution()
          << sysInfo.getKernel() << storage.fileSystemType() << sysInfo.getCpuModel() << sys.currentCpuArchitecture() << sysInfo.getCpuSpeed()
          << sysInfo.getCpuCore() << CPrime::FileFunc::formatSize(im->getMemTotal()) << scr << sysInfo.getBootTime() << sysInfo.getUptime() << QT_VERSION_STR;

    ui->infoLeft->clear();
    ui->infoRight->clear();

    int count = left.count();
    QString sep = "\n";

    for (int i = 0; i < count; i++) {
        if (i + 1 == count) {
            sep = "";
        }

        ui->infoLeft->setText(ui->infoLeft->text() + left[i] + sep);
        ui->infoRight->setText(ui->infoRight->text() + ": " + right[i] + sep);
    }
}

void pgeneral::updateCpuBar()
{
    int cpuCount = im->getCpuCoreCount();
    int cpuUsedPercent = 0;

    foreach (int i, im->getCpuPercents()) {
        cpuUsedPercent += i;
    }

    cpuUsedPercent = static_cast<int>(cpuUsedPercent / cpuCount);

    ui->cpu->setText(QString::number(cpuUsedPercent) + "%" + "<sup>CPU</sup>");

}

void pgeneral::updateMemoryBar()
{
	im->updateMemoryInfo();
    int memUsedPercent = 0;

    if (im->getMemTotal()) {
        memUsedPercent = static_cast<int>((static_cast<double>(im->getMemUsed()) / static_cast<double>(im->getMemTotal())) * 100.0);
    }

    ui->ram->setText(QString::number(memUsedPercent) + "%" + "<sup>RAM</sup>");
}

void pgeneral::updateNetworkBar()
{
    quint64 RecvBytes = im->getRXbytes() / 8;
    quint64 TrnsBytes = im->getTXbytes() / 8;

    quint64 RecvSpeed = prevRXBytes ? RecvBytes - prevRXBytes : 0;
    quint64 TrnsSpeed = prevTXBytes ? TrnsBytes - prevTXBytes : 0;

    prevRXBytes = RecvBytes;
    prevTXBytes = TrnsBytes;

    ui->net->setText("<p><sub>NET   </sub>"+ CPrime::FileFunc::formatSize(TrnsSpeed) +"<sup>UP</sup></p><p>   "+CPrime::FileFunc::formatSize(RecvSpeed)+"<sub>DN</sub></p>");
}

void pgeneral::updateStorageBar()
{
    QStorageInfo s(QDir::root());

    qint64 total = s.bytesTotal();
    qint64 free = s.bytesFree();
    qint64 used = total - free;

    int percent = (used / static_cast<double>(total)) * 100;
    ui->storage->setText(QString::number(percent) + "%" + "<sup>DISK</sup>");

}

#-------------------------------------------------
#
# Project created by QtCreator 2018-08-20T09:12:54
#
#-------------------------------------------------

QT      += core gui widgets dbus

TARGET   = corestats
TEMPLATE = app

VERSION  = 2.9.0

# Library section
unix:!macx: LIBS += -lcprime
unix:!macx: LIBS += -L/usr/lib -lcsys
LIBS += -lsensors

# Disable warnings, enable threading support and c++11
CONFIG  += thread silent build_all c++11

# Definetion section
DEFINES += QT_DEPRECATED_WARNINGS QT_DISABLE_DEPRECATED_BEFORE=0
DEFINES += "HAVE_POSIX_OPENPT"

# Build section
BUILD_PREFIX = $$(CA_BUILD_DIR)

isEmpty( BUILD_PREFIX ) {
        BUILD_PREFIX = ./build
}

MOC_DIR       = $$BUILD_PREFIX/moc-qt5
OBJECTS_DIR   = $$BUILD_PREFIX/obj-qt5
RCC_DIR	      = $$BUILD_PREFIX/qrc-qt5
UI_DIR        = $$BUILD_PREFIX/uic-qt5

# Disable QDebug on Release build
CONFIG(release, debug|release):DEFINES += QT_NO_DEBUG_OUTPUT

unix {
        isEmpty(PREFIX) {
                PREFIX = /usr
        }
        BINDIR = $$PREFIX/bin

        target.path = $$BINDIR

        desktop.path  = $$PREFIX/share/applications/
        desktop.files = "corestats.desktop"

        icons.path  = /usr/share/icons/hicolor/scalable/apps/
        icons.files = corestats.svg

        INSTALLS += target icons desktop
}


FORMS += \
    pbattery.ui \
    pdisplay.ui \
    pdrives.ui \
    pgeneral.ui \
    pnetwork.ui \
    presources.ui \
    corestats.ui \
    psysmonitor.ui

HEADERS += \
    pbattery.h \
    pdisplay.h \
    pdrives.h \
    pgeneral.h \
    pnetwork.h \
    presources.h \
    corestats.h \
    psysmonitor.h \
    usagegraph.h

SOURCES += \
    main.cpp \
    pbattery.cpp \
    pdisplay.cpp \
    pdrives.cpp \
    pgeneral.cpp \
    pnetwork.cpp \
    presources.cpp \
    corestats.cpp \
    psysmonitor.cpp \
    usagegraph.cpp

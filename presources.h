/*
    *
    * This file is a part of CoreStats.
    * A real time system resource viewer for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#ifndef PRESOURCES_H
#define PRESOURCES_H

#include <QWidget>
#include <QBasicTimer>

#include <csys/info_manager.h>


class UsageGraph;

class presources : public QWidget {
	Q_OBJECT

public:
	explicit presources(QWidget *parent = nullptr);
	~presources();

private slots:
	void zoomChart();

protected:
    void timerEvent(QTimerEvent *tEvent);

private:
    UsageGraph *mPlotCpu;
    UsageGraph *mPlotCpuLoadAvg;
    UsageGraph *mPlotDiskIO;
    UsageGraph *mPlotMemory;
    UsageGraph *mPlotNetwork;
	InfoManager *im;
	QBasicTimer *mTimer;
	quint64 time;

	quint64 prevRXBytes = 0, prevTXBytes = 0;
	quint64 prevRBytes = 0, prevWBytes = 0;
	quint64 prevDiskMax = 0;
	quint64 prevNetMax = 0;

	void init();

	void updateCpuChart();
	void updateCpuLoadAvg();
	void updateDiskReadWrite();
	void updateMemoryChart();
	void updateNetworkChart();
};

#endif // PRESOURCES_H

/*
    *
    * This file is a part of CoreStats.
    * A real time system resource viewer for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

// Local Headers
#include "usagegraph.h"
#include <cprime/cprime.h>

/* Show data of 5 minutes, updated every 1s */
const int MAX_POINTS = 300;

static QList<QColor> colors = {
    0x2ECC71, 0xD35400, 0x3498DB, 0xF1C40F, 0xE67E22,
    0x1ABC9C, 0x9B59B6, 0x34495E, 0xE74C3C, 0xC0392B,
    0x8E44AD, 0xFF8F00, 0xEF6C00, 0x4E342E, 0x424242,
    0x5499C7, 0x58D68D, 0xCD6155, 0xF5B041, 0x566573
};

/*
    *
    * UsagePlot - Draw the graph for resource usages
    *
*/

UsagePlot::UsagePlot(int resources, QWidget *parent) : QWidget(parent)
{
    /* Init values for resources */
    mResources = resources;

    for (int r = 0; r < resources; r++) {
        UsageValues values;

        for (int v = 0; v < MAX_POINTS; v++) {
            values << 0;
        }

        mResValMap[ r ] = values;
    }

    /* SizePolicy */
    setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
}

void UsagePlot::setYRange(double min, double max)
{
    minY = min;
    maxY = max;

    repaint();
}

void UsagePlot::addValues(UsageValues values)
{
    for (int r = 0; r < mResources; r++) {
        mResValMap[ r ].removeFirst();
        mResValMap[ r ] << values[ r ];
    }

    repaint();
}

void UsagePlot::paintEvent(QPaintEvent *pEvent)
{
    int w = width();
    int h = height();

    double dw = 1.0 * w / MAX_POINTS;

    QPainter painter(this);
    painter.setRenderHints(QPainter::Antialiasing);
    painter.fillRect(rect(), Qt::transparent);

    painter.setPen(Qt::gray);
    painter.drawRect(QRectF(0, 0, w, h));

    for (int r = 0; r < mResources; r++) {
        painter.save();

        painter.setPen(QPen(colors[ r ], 2));

        for (int i = 0; i < MAX_POINTS - 1; i++) {
            QPointF p1(dw * i, h - h * (mResValMap[ r ][ i ] - minY) / (maxY - minY));
            QPointF p2(dw * (i + 1), h - h * (mResValMap[ r ][ i + 1 ] - minY) / (maxY - minY));
            painter.drawLine(p1, p2);
        }

        painter.restore();
    }

    painter.end();

    pEvent->accept();
}

/*
    *
    * UsageGraph - Complete graph with x, y scales and legend
    *
*/

UsageGraph::UsageGraph(QString title, QString units, int resources, QWidget *parent) : QWidget(parent)
{
    mResources = resources;

    mTitle = title;
    mUnits = units;

    titleCB = new QToolButton(this);
    titleCB->setText(title + " (" + units + ")");
    titleCB->setCheckable(true);
    titleCB->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);


    titleCB->setChecked(false);
    connect(titleCB, SIGNAL(clicked(bool)), this, SIGNAL(zoomChart()));

    plot = new UsagePlot(resources, this);

    minY = 0;
    maxY = 100;
    plot->setYRange(minY, maxY);

    /* Init x labels */
    QStringList xLbl = QStringList() << "5m" << "4m" << "3m" << "2m" << "1m" << "now";

    for (int i = 0; i < 6; i++) {
        QLabel *lbl = new QLabel();
        lbl->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
        lbl->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
        lbl->setText(xLbl[ i ]);

        xLabels << lbl;
    }

    /* Init y labels */
    double step = ( maxY - minY ) / 4;

    for ( int i = 0; i < 5; i++ ) {
        QLabel *lbl = new QLabel();
        lbl->setAlignment( Qt::AlignRight | Qt::AlignVCenter );
        lbl->setSizePolicy( QSizePolicy::Minimum, QSizePolicy::Minimum );
        lbl->setText( QString::number( static_cast<int>( minY + step * i ) ) );

        yLabels.prepend( lbl );
    }

    /* Init legends */
    for (int i = 0; i < resources; i++) {
        QLabel *lbl = new QLabel();
        lbl->setAlignment(Qt::AlignCenter);
        lbl->setText(QString("<font color='%1'>Graph %2</font>").arg(colors[ i ] .name()).arg(i + 1));

        legends << lbl;
    }

	/* Add y labels to layout */
	QVBoxLayout *yLyt = new QVBoxLayout();
	for ( int i = 0; i < 5; i++ ) {
		yLyt->addWidget( yLabels[ i ] );

		if ( i < 4 ) {
			yLyt->addStretch();
		}
	}

	/* Add x labels to layout */
	QHBoxLayout *xLyt = new QHBoxLayout();
	for ( int i = 0; i < 6; i++ ) {
		xLyt->addWidget( xLabels[ i ] );

		if ( i < 5 ) {
			xLyt->addStretch();
		}
	}

    QHBoxLayout *legLyt = new QHBoxLayout();

    for (int i = 0; i < resources; i++) {
        legLyt->addWidget(legends[ i ]);
    }

    QGridLayout *baseLyt = new QGridLayout();
    baseLyt->setSpacing(5);
    baseLyt->addWidget(titleCB, 0, 0, 1, 2);
    baseLyt->addLayout(yLyt, 1, 0);
    baseLyt->addWidget(plot, 1, 1);
    baseLyt->addLayout(xLyt, 2, 1);
    baseLyt->addLayout(legLyt, 3, 0, 1, 2);

    setLayout(baseLyt);
}

void UsageGraph::setYRange(double /* min = 0 */, double max)
{
    maxY = max;

    double fMax = CPrime::FileFunc::formatSizeRaw(static_cast<quint64>(max));
    mUnits = CPrime::FileFunc::formatSizeStr(static_cast<quint64>(max));

    if (mUnits != "%") {
        titleCB->setText(mTitle + " (" + mUnits + ")");
    }

    double step = fMax / 4;

    for ( int i = 0; i < 5; i++ ) {
        yLabels[ i ]->setText( QString::number( minY + step * ( 4 - i ), 'f', 1 ) );
    }

    plot->setYRange(0, max);
}

void UsageGraph::setLegend(QStringList lgnds)
{
    for (int i = 0; i < mResources; i++) {
        legends[ i ]->setText(QString("<font color='%1'><b>%2</b></font>").arg(colors[ i ].name()).arg(lgnds.value(i)));
    }
}

void UsageGraph::addValues(UsageValues values)
{
    plot->addValues(values);
}

bool UsageGraph::zoom()
{
    return titleCB->isChecked();
}

void UsageGraph::resizeEvent( QResizeEvent *rEvent ) {

    rEvent->accept();

    if ( height() < 200 ) {
        yLabels[ 1 ]->hide();
        yLabels[ 3 ]->hide();
    }

    else {
        yLabels[ 1 ]->show();
        yLabels[ 3 ]->show();
    }
};

/*
    *
    * This file is a part of CoreStats.
    * A real time system resource viewer for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/


#include <QToolButton>
#include <QScroller>

#include <cprime/themefunc.h>

#include "pbattery.h"
#include "pdisplay.h"
#include "pdrives.h"
#include "pgeneral.h"
#include "pnetwork.h"
#include "presources.h"
#include "psysmonitor.h"
#include "corestats.h"
#include "ui_corestats.h"


corestats::corestats(QWidget *parent): QWidget(parent), ui(new Ui::corestats)
{
    ui->setupUi(this);

    loadSettings();
    startSetup();
    on_Bgeneral_clicked();
}

corestats::~corestats()
{
    delete ui->pages;
    delete ui;
}

/**
 * @brief Setup ui elements
 */
void corestats::startSetup()
{
    // set stylesheet from style.qrc
    ui->sideView->setStyleSheet(CPrime::ThemeFunc::getSideViewStyleSheet());
    ui->sideView->setFixedWidth(sideViewSize);

    ui->menu->setVisible(0);
    ui->appTitle->setAttribute(Qt::WA_TransparentForMouseEvents);
    ui->appTitle->setFocusPolicy(Qt::NoFocus);
    this->resize(800, 500);

    if (touch) {
        this->setWindowState(Qt::WindowMaximized);
        QScroller::grabGesture(ui->scrollArea, QScroller::LeftMouseButtonGesture);

        if (not sm->value("CoreApps", "TabletMode")) {
            ui->menu->setIconSize(toolBarIconSize);

            connect(ui->menu, &QToolButton::clicked, this, &corestats::showSideView);
            connect(ui->appTitle, &QToolButton::clicked, this, &corestats::showSideView);

            ui->sideView->setVisible(0);
            ui->menu->setVisible(1);
        }
    }

    // all toolbuttons icon size in sideView
    for (QToolButton *b : ui->sideView->findChildren<QToolButton *>()) {
        if (b) {
            b->setIconSize(sideViewIconSize);
        }
    }

    if (!isExperimental) {
        ui->Bnetwork->setVisible(false);
    }

    // Hide the battery button if there is no battery
    if (not BatteryManager::instance()->batteries().count()) {
        ui->Bbattery->setVisible(0);
    }
}

/**
 * @brief Loads application settings
 */
void corestats::loadSettings()
{
    sideViewIconSize = sm->value("CoreApps", "SideViewIconSize");
    listViewIconSize = sm->value("CoreApps", "ListViewIconSize");
    toolBarIconSize = sm->value("CoreApps", "ToolBarIconSize");
    sideViewSize = sm->value("CoreApps", "SideViewSize");
    touch = sm->value("CoreApps", "TouchMode");
    isExperimental = sm->value("CoreApps", "EnableExperimental");
}

void corestats::closeEvent(QCloseEvent *event)
{
    for (int i = 0; i < ui->pages->count(); i++) {
        QObjectList list = ui->pages->widget(i)->children();

        if (list.count() == 2) {
            if (list.at(1)) {
                static_cast<QWidget *>(list.at(1))->close();
            }
        }
    }

    event->accept();
}

void corestats::showSideView()
{
    if (touch) {
        if (ui->sideView->isVisible()) {
            ui->sideView->setVisible(0);
        } else {
            ui->sideView->setVisible(1);
        }
    }
}

void corestats::pageClick(QToolButton *btn, int i, QString title)
{
    // first do this to get all the space for contents
    if (touch) {
        if (not sm->value("CoreApps", "TabletMode")) {
            ui->sideView->setVisible(0);
        }
    }

    // all button checked false
    for (QToolButton *b : ui->sideView->findChildren<QToolButton *>()) {
        b->setChecked(false);
    }

    btn->setChecked(true);
    ui->selectedsection->setText(title);
    ui->pages->setCurrentIndex(i);
    this->setWindowTitle(title + " - CoreStats");
}

void corestats::on_Bgeneral_clicked()
{
    if (ui->pages->widget(0)->children().count() < 2) {
        pgeneral *infoo = new pgeneral(this);
        ui->pGeneralLayout->addWidget(infoo);
    }

    pageClick(ui->Bgeneral, 0, tr("General"));
}

void corestats::on_Bdrives_clicked()
{
    if (ui->pages->widget(1)->children().count() < 2) {
        pDrives *drives = new pDrives(this);
        ui->pDriveLayout->addWidget(drives);
    }

    pageClick(ui->Bdrives, 1, tr("Drives"));
}

void corestats::on_Bbattery_clicked()
{
    if (ui->pages->widget(2)->children().count() < 2) {
        pBattery *battery = new pBattery(this);
        ui->pBatteryLayout->addWidget(battery);
    }

    pageClick(ui->Bbattery, 2, tr("Battery"));
}

void corestats::on_Bnetwork_clicked()
{
    if (ui->pages->widget(3)->children().count() < 2) {
        pnetwork *network = new pnetwork(this);
        ui->pNetworkLayout->addWidget(network);
    }

    pageClick(ui->Bnetwork, 3, tr("Network"));
}

void corestats::on_Bresource_clicked()
{
    if (ui->pages->widget(4)->children().count() < 2) {
        presources *resourcePage = new presources(this);
        // QScrollArea *base = new QScrollArea(this);
        // base->setFrameShape(QFrame::NoFrame);
        // base->setWidgetResizable(true);
        // base->setWidget(resourcePage);
        ui->pResourceLayout->addWidget(resourcePage);
    }

    pageClick(ui->Bresource, 4, tr("Resource"));
}

void corestats::on_Bsysmonitor_clicked()
{
    if (ui->pages->widget(5)->children().count() < 2) {
        pSysMonitor *sysMonitor = new pSysMonitor(this);
        ui->pSysMonitorLayout->addWidget(sysMonitor);
    }

    pageClick(ui->Bsysmonitor, 5, tr("System Monitor"));
}

void corestats::on_Bdisplay_clicked()
{
    if (ui->pages->widget(6)->children().count() < 2) {
        pDisplay *display = new pDisplay(this);
        ui->pDisplayLayout->addWidget(display);
    }

    pageClick(ui->Bdisplay, 6, tr("Display"));
}

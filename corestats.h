/*
    *
    * This file is a part of CoreStats.
    * A real time system resource viewer for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/


#ifndef CORESTATS_H
#define CORESTATS_H

#include <QWidget>
#include <QCloseEvent>

#include <cprime/settingsmanage.h>

class QToolButton;

namespace Ui {
    class corestats;
}

class corestats : public QWidget {
    Q_OBJECT

public:
    explicit corestats(QWidget *parent = nullptr);
    ~corestats();

private slots:
    void on_Bresource_clicked();
    void on_Bdrives_clicked();
    void on_Bbattery_clicked();
    void on_Bnetwork_clicked();
    void on_Bgeneral_clicked();
    void on_Bdisplay_clicked();
    void showSideView();

    void on_Bsysmonitor_clicked();

protected:
    void closeEvent(QCloseEvent *event) override;

private:
    Ui::corestats *ui;
    settingsManage *sm = settingsManage::initialize();
    bool           touch;
    int            sideViewSize;
    QSize          sideViewIconSize, listViewIconSize, toolBarIconSize;
    bool isExperimental;

    void pageClick(QToolButton *btn, int i, QString title);
    void loadSettings();
    void startSetup();
};

#endif // CORESTATS_H

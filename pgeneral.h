/*
    *
    * This file is a part of CoreStats.
    * A real time system resource viewer for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/


#ifndef PGENERAL_H
#define PGENERAL_H

#include <QWidget>
#include <QBasicTimer>

#include <csys/info_manager.h>


class UsageBar;

namespace Ui {
	class pgeneral;
}

class pgeneral : public QWidget {
    Q_OBJECT

public:
    explicit pgeneral(QWidget *parent = nullptr);
    ~pgeneral();

protected:
    void timerEvent(QTimerEvent *event);

private slots:
    void updateInfo();
    void systemInformationInit();

    void updateCpuBar();
    void updateMemoryBar();
    void updateNetworkBar();
    void updateStorageBar();

private:
    Ui::pgeneral *ui;
    QBasicTimer *timer;
    InfoManager *im;

    quint64 prevRXBytes = 0, prevTXBytes = 0;
    QString cpuCore, cpuModel, cpuSpeed, username;
};

#endif // PGENERAL_H

/*
    *
    * This file is a part of CoreStats.
    * A real time system resource viewer for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/


#include <QTime>
#include <QStringListModel>

#include <cprime/cprime.h>

#include "pbattery.h"
#include "ui_pbattery.h"

pBattery::pBattery(QWidget *parent) : QWidget(parent), ui(new Ui::pBattery)
{
    ui->setupUi(this);
    ui->infoLeft->setStyleSheet("QLabel {background-color: transparent;}");
    ui->infoRight->setStyleSheet("QLabel {background-color: transparent;}");

    batman = BatteryManager::instance();

    setupBatteryPage();

    timer = new QBasicTimer;
    timer->start(1000, this);
}

pBattery::~pBattery()
{
    batman->deleteLater();
    delete ui;
}

void pBattery::reload()
{
    int index = ui->batteriesList->currentIndex();
    on_batteriesList_currentIndexChanged(index);
    Battery bat = batman->batteries().at(index);
    unsigned int curr = bat.value("State").toUInt();

    if (curr != ostate) {
        updateList(bat);
        ostate = curr;
    }
}

void pBattery::timerEvent(QTimerEvent *event)
{
    if (event->timerId() == timer->timerId()) {
        reload();
    }
}

void pBattery::setupBatteryPage()
{
	ui->batteriesList->clear();

    Q_FOREACH (Battery bat, batman->batteries()) {
        ui->batteriesList->addItem(bat.name());
	}

    on_batteriesList_currentIndexChanged(0);
    updateList(batman->batteries().at(0));
}

void pBattery::on_refresh_clicked()
{
    on_batteriesList_currentIndexChanged(ui->batteriesList->currentIndex());
    updateList(batman->batteries().at(ui->batteriesList->currentIndex()));
}

void pBattery::on_batteriesList_currentIndexChanged(int index)
{
    Battery bat = batman->batteries().at(index);

    ui->lvlProgress->setValue(bat.value("Percentage").toInt());
    ui->capacityProgress->setValue(bat.value("Capacity").toInt());
}

void pBattery::updateList(Battery bat)
{
    ui->icon->setIcon(QIcon::fromTheme(bat.value("IconName").toString()));

    QString status;
    QString time1;

    QStringList left;
    QStringList right;

    QStringList list;
    list << "NativePath" << "Path" << "Model" << "Vendor" << "Technology" << "Serial" << "Type"
         << "Percentage" << "Capacity" << "Energy" << "EnergyEmpty" << "EnergyFull" << "EnergyFullDesign" << "EnergyRate" << "Voltage"
         << "State" << "WarningLevel" << "TimeToFull" << "TimeToEmpty" << "UpdateTime"
         << "PowerSupply" << "HasHistory" << "HasStatistics" << "IsRechargeable" << "IsPresent" << "Online"
         /*<< "Luminosity" << "Temperature"*/;


    left << "Native Path" << "Path" << "Model" << "Vendor" << "Technology" << "Serial" << "Type of power source"
         << "Percentage" << "Capacity" << "Energy" << "Energy Empty" << "Energy Full" << "Energy Full Design" << "Energy Rate" << "Voltage"
         << "State" << "Warning Level" << "Battery Level" << "Time To Full" << "Time To Empty" << "Update Time"
         << "Power Supply" << "Has History" << "Has Statistics" << "Is Rechargeable" << "Is Present" << "Online"
         /*<< "Luminosity" << "Temperature"*/;

    int count = left.count();

    for (int i = 0; i < count; i++) {
        QString curr = left[i];

        QVariant val = bat.value(QString(curr).replace(" ", ""));

        if (curr == "Technology") {
            unsigned int tech = val.toUInt();
            QString techtxt = "";

            switch (tech) {
                case 0:
                    techtxt = "Unknown";
                    break;

                case 1:
                    techtxt = "Lithium ion";
                    break;

                case 2:
                    techtxt = "Lithium polymer";
                    break;

                case 3:
                    techtxt = "Lithium iron phosphate";
                    break;

                case 4:
                    techtxt = "Lead acid";
                    break;

                case 5:
                    techtxt = "Nickel cadmium";
                    break;

                case 6:
                    techtxt = "Nickel metal hydride";
                    break;

                default:
                    techtxt = "Invalid";
                    break;
            }

            right << techtxt;

            continue;
        }

        if (curr == "Type of power source") {
            unsigned int type = val.toUInt();
            QString typetxt = "";

            switch (type) {
                case 0:
                    typetxt = "Unknown";
                    break;

                case 1:
                    typetxt = "Line Power";
                    break;

                case 2:
                    typetxt = "Battery";
                    break;

                case 3:
                    typetxt = "UPS";
                    break;

                case 4:
                    typetxt = "Monitor";
                    break;

                case 5:
                    typetxt = "Mouse";
                    break;

                case 6:
                    typetxt = "Keyboard";
                    break;

                case 7:
                    typetxt = "PDA";
                    break;

                case 8:
                    typetxt = "Phone";
                    break;

                default:
                    typetxt = "Invalid";
                    break;
            }

            right << typetxt;
            continue;
        }

        if (curr == "State") {
            unsigned int state = val.toUInt();
            QString statetxt = "";

            switch (state) {
                case 0:
                    statetxt = "Unknown";
                    break;

                case 1:
                    statetxt = "Charging";
                    break;

                case 2:
                    statetxt = "Discharging";
                    break;

                case 3:
                    statetxt = "Empty";
                    break;

                case 4:
                    statetxt = "Fully Charged";
                    break;

                case 5:
                    statetxt = "Pending charge";
                    break;

                case 6:
                    statetxt = "Pending discharge";
                    break;

                default:
                    statetxt = "Invalid";
                    break;
            }

            right << statetxt;
            status = statetxt;

            continue;
        }

        if (curr == "Warning Level") {
            unsigned int wlev = val.toUInt();
            QString wlevtxt = "";

            switch (wlev) {
                case 0:
                    wlevtxt = "Unknown";
                    break;

                case 1:
                    wlevtxt = "None";
                    break;

                case 2:
                    wlevtxt = "Discharging (only for UPSes)";
                    break;

                case 3:
                    wlevtxt = "Low";
                    break;

                case 4:
                    wlevtxt = "Critical";
                    break;

                case 5:
                    wlevtxt = "Action";
                    break;

                default:
                    wlevtxt = "Invalid";
                    break;
            }

            right << wlevtxt;
            continue;
        }

        if (curr == "Battery Level") {
            unsigned int blev = val.toUInt();
            QString blevtxt = "";

            switch (blev) {
                case 0:
                    blevtxt = "Unknown";
                    break;

                case 1:
                    blevtxt = "None";
                    break;

                case 2:
                    blevtxt = "Discharging (only for UPSes)";
                    break;

                case 3:
                    blevtxt = "Low";
                    break;

                case 4:
                    blevtxt = "Critical";
                    break;

                case 5:
                    blevtxt = "Action";
                    break;

                default:
                    blevtxt = "Invalid";
                    break;
            }

            right << blevtxt;
            continue;
        }

        if (curr == "Update Time") {
            QDateTime last = QDateTime::fromMSecsSinceEpoch(val.toLongLong() * 1000);
            QDateTime curr = QDateTime::currentDateTime();
            right << last.toString() + " (" + QString("%1 seconds ago").arg(last.secsTo(curr)) + ")";
            continue;
        }

        QString extra = "";

        if (val.type() == QVariant::Double) {
            if (curr == "Percentage" || curr == "Capacity") {
                extra = "%";
            } else if (curr == "Energy Rate") {
                extra = " W";
            } else if (curr == "Voltage") {
                extra = " V";
            } else {
                extra = " Wh";
            }

            right << QString::number(val.toDouble()) + extra;
        } else if (val.type() == QVariant::LongLong) {
            QString tmp = QDateTime(QDate::currentDate(), QTime(0, 0, 0, 0)).addSecs(val.toLongLong()).time().toString();

            if (val.toLongLong() > 0) {
                time1 = tmp;
            }

            right << tmp;
        } else {
            right << val.toString();
        }
    }

    if (status == "Fully Charged") {
        ui->statusEdit->setText(status);
    } else {
        ui->statusEdit->setText(status + " (" + time1 + ")");
    }

    ui->infoLeft->clear();
    ui->infoRight->clear();

    QString sep = "\n";

    for (int i = 0; i < count; i++) {
        if (i + 1 == count) {
            sep = "";
        }

        ui->infoLeft->setText(ui->infoLeft->text() + left[i] + sep);
        ui->infoRight->setText(ui->infoRight->text() + ": " + right[i] + sep);
    }
}

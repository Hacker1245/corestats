/*
    *
    * This file is a part of CoreStats.
    * A real time system resource viewer for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/


#include <QProcess>
#include <QListView>
#include <QScreen>
#include <QStringListModel>
#include <QSpacerItem>
#include <QtMath>
#include <QGroupBox>
#include <QLabel>

#include <cprime/cprime.h>

#include "pdisplay.h"
#include "ui_pdisplay.h"


pDisplay::pDisplay(QWidget *parent) : QWidget(parent), ui(new Ui::pDisplay)
{
    ui->setupUi(this);
    setupDisplayPage();
}

pDisplay::~pDisplay()
{
    delete ui;
}

bool pDisplay::touchSupport()
{
    QProcess proc;
    proc.start("udevadm", QStringList() << "info" << "--export-db");
    proc.waitForFinished(-1);

    QString output = QString::fromLocal8Bit(proc.readAllStandardOutput() + '\n' + proc.readAllStandardError());

    if (output.contains("ID_INPUT_TOUCHSCREEN=1")) {
        return true;
    }

    return false;
}

void pDisplay::setupDisplayPage()
{
    QStringList left;
    left << "Name" << "Size " << "Manufacturer" << "Model"
         << "Serial Number" << "Refresh Rate" << "Default Resolution"
         << "Set Resolution" << "Physical Dots Per Inch "
         << "Physical Size" <<  "Screen Size" << "Default Orientation"
         << "Set Orientation" << "Touch Support";

    for (int i = 0; i < qApp->screens().count(); i++) {

        QSize s = qApp->screens()[i]->size();
        QString size(tr("(%1,%2)").arg(s.width()).arg(s.height()));

        QSize a = qApp->screens()[i]->availableVirtualSize();
        QString availableVS(tr("(%1,%2)%3").arg(a.width()).arg(a.height()).arg(" px"));

        QRect g = qApp->screens()[i]->geometry();
        QString geometry(tr("(%1,%2)%3").arg(g.width()).arg(g.height()).arg(" px"));

        QSizeF py = qApp->screens()[i]->physicalSize();
        QString physicalSize(tr("(%1,%2)%3").arg(py.width()).arg(py.height()).arg(" millimetre"));

        // screen size in inches
        int screenSize(static_cast<int>(qSqrt(qPow(py.width(), 2) + qPow(py.height(), 2)) * 0.03937008));


        // screen orientation
        QString defultOrientation = "";

        if (qApp->screens()[i]->nativeOrientation() == Qt::LandscapeOrientation) {
            defultOrientation = "Landscape";
        } else {
            defultOrientation = "Portrait";
        }

        QString setOrientation = "";

        if (qApp->screens()[i]->primaryOrientation() == Qt::LandscapeOrientation) {
            setOrientation = "Landscape";
        } else {
            setOrientation = "Portrait";
        }

        QStringList right;

        QLabel *infoLeft = new QLabel();
        infoLeft->setFocusPolicy(Qt::NoFocus);
        QLabel *infoRight = new QLabel();
        infoRight->setFocusPolicy(Qt::NoFocus);

        QWidget *w = new QWidget();
        w->setMinimumHeight(300);
        w->setFocusPolicy(Qt::NoFocus);
        QString title = ("Screen : " + QString::number(i + 1));

        QGroupBox *b = new QGroupBox(tr("title"));
        QVBoxLayout *v = new QVBoxLayout();
        QHBoxLayout *h = new QHBoxLayout();


        right << qApp->screens()[i]->name() << size;
#if QT_VERSION >= QT_VERSION_CHECK( 5, 9, 0 )
        right << qApp->screens()[i]->manufacturer();
        right << qApp->screens()[i]->model();
        right << qApp->screens()[i]->serialNumber();
#else
        right << "Unknown manufacturer";
        right << "Unknwon model";
        right << "Unknwon";
#endif

        right << QString::number(qApp->screens()[i]->refreshRate()) + " Hz" << availableVS << geometry
              << QString::number(qApp->screens()[i]->physicalDotsPerInch()) << physicalSize
              << QString::number(screenSize) + " inches"
              << defultOrientation << setOrientation << (touchSupport() ? "Yes" : "No");


        infoLeft->clear();
        infoRight->clear();

        int count = left.count();
        QString sep = "\n";

        for (int i = 0; i < count; i++) {
            if (i + 1 == count) {
                sep = "";
            }

            infoLeft->setText(infoLeft->text() + left[i] + sep);
            infoRight->setText(infoRight->text() + ": " + right[i] + sep);
        }

        h->addWidget(infoLeft);
        h->addWidget(infoRight);
        b->setTitle(title);
        h->setContentsMargins(9, 9, 9, 9);
        b->setLayout(h);

        v->addWidget(b);
        w->setLayout(v);

        ui->lay->addWidget(w);

        right.clear();
    }

    QSpacerItem *verticalSpacer = new QSpacerItem(10, 10, QSizePolicy::Minimum, QSizePolicy::Expanding);
    ui->lay->addItem(verticalSpacer);
}

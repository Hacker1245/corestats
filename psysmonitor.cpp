/*
    *
    * This file is a part of CoreStats.
    * A real time system resource viewer for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#include <QFile>
#include <QTextStream>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QDebug>

#include <math.h>
#include <sensors/sensors.h>
#include <sensors/error.h>

#include "psysmonitor.h"
#include "ui_psysmonitor.h"


pSysMonitor::pSysMonitor(QWidget *parent) :QWidget(parent),ui(new Ui::pSysMonitor)
{
    ui->setupUi(this);

    im = InfoManager::ins();
    timer = new QBasicTimer;
    timer->start(1000, this);

    startSetup();
}

pSysMonitor::~pSysMonitor()
{
    delete ui;
}

void pSysMonitor::timerEvent(QTimerEvent *event)
{
    if (event->timerId() == timer->timerId()) {
        updateFreqs();
    }
}

QStringList pSysMonitor::cpuFrequencies()
{
    return im->getCpuFrequenciesStr();
}

void pSysMonitor::startSetup()
{
    QStringList cpuFreqs = cpuFrequencies();

    int row = 0;
    int col = 0;

    Q_FOREACH (QString str, cpuFreqs) {
        QLabel *lbl = new QLabel(str);
        lbl->setWordWrap(1);

        if (col % 4 == 0) {
            col = 0;
            row++;
        }

        ui->cpuFreqsLayout->addWidget(lbl, row, col);

        col++;
    }

    row = 0;
    col = 0;

    QStringList sensorsData = sensors();

    Q_FOREACH (QString str, sensorsData) {
        QLabel *lbl = new QLabel(str);
        lbl->setWordWrap(1);

        if (col % 2 == 0) {
            col = 0;
            row++;
        }

        ui->sensorsLayout->addWidget(lbl, row, col);

        col++;
    }
}

QStringList pSysMonitor::sensors()
{
    QStringList dataList;

    char buf[256];
    int chipNumber, temp;

    const sensors_chip_name *chip;
    const sensors_subfeature *sub;
    const sensors_feature *feature;
    const char *adap = NULL;

    if (int err = sensors_init(NULL)) {
        sensors_strerror(err);
        return QStringList();
    } else {
        chipNumber = 0;

        while ((chip = sensors_get_detected_chips(NULL, &chipNumber))) {
            QString data = "";

            if (sensors_snprintf_chip_name(buf, 256, chip) < 0) {
                sprintf(buf, "%i", chipNumber);
            }

            data += "Chip Name: " + QString::fromLocal8Bit(buf) + "\n";

            adap = sensors_get_adapter_name(&chip->bus);

            if (adap) {
                data += "Adapter Name: " + QString::fromLocal8Bit(adap) + "\n";
            }

            temp = 0;

            while ((feature = sensors_get_features(chip, &temp))) {
                char *str = sensors_get_label(chip, feature);
                double val = 0.0;
                sub = sensors_get_subfeature(chip, feature, (sensors_subfeature_type)(((int)feature->type) << 8));
                sensors_get_value(chip, sub->number, &val);
                data += QString::fromLocal8Bit(str) + ": " + QString::number(val) + '\n';
            }

            dataList.append(data);
        }
    }

    return dataList;
}

void pSysMonitor::updateFreqs()
{
    QStringList cpuFreqs = cpuFrequencies();

    if (!cpuFreqs.count()) {
        return;
    }

    QObjectList objList = ui->cpuFreqs->children();

    if (!objList.count()) {
        return;
    }

    int i = 0;

    Q_FOREACH (QObject *obj, objList) {
        QLabel *lbl = qobject_cast<QLabel *>(obj);

        if (lbl) {
            lbl->setText(cpuFreqs[i++]);
        }
    }
}

void pSysMonitor::updateSensors()
{
    QStringList sensorsData = sensors();

    if (!sensorsData.count()) {
        return;
    }

    QObjectList objList = ui->sensors->children();

    if (!objList.count()) {
        return;
    }

    int i = 0;

    Q_FOREACH (QObject *obj, objList) {
        QLabel *lbl = qobject_cast<QLabel *>(obj);

        if (lbl) {
            lbl->setText(sensorsData[i++]);
        }
    }

    sensorsData.clear();
    objList.clear();
}

void pSysMonitor::on_refreshSensor_clicked()
{
    updateSensors();
}
